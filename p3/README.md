# Project 3 (P3): Electric Vehicle Sales

## Clarifications/Corrections:

* None yet.

**Find any issues?** Report to us:

- Kincannon Wilson <kgwilson2@wisc.edu>
- Chaithanya Naik Mude <cmude@wisc.edu>
- Hakan Dingenc <dingenc@wisc.edu>

## Instructions:

In this project, we will focus on function calls, function definitions, default arguments, and simple arithmetic operations. To start, create a `p3` directory, and download `p3.ipynb`, `project.py`, `test.py`, and `car_sales_data.csv`. For each project, we will have you download a new `test.py` - please make sure to download the correct files for the current project.

**Note:** Please go through [lab-p3](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p3) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p3.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**. 

After you've downloaded the file to your `p3` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p3.ipynb`, `project.py`, `test.py`, and `car_sales_data.csv` are listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the `jupyter notebook` command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/p3/rubric.md), to ensure that you don't lose points during code review.
- You must **save your notebook file** before you run the cell containing **export**.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P3 assignment.

